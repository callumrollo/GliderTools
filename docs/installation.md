Installation
============

##### PyPI
To install the core package run: `pip install glidertools`.

##### GitLab
1. Clone glidertools to your local machine: `git clone https://gitlab.com/socco/GliderTools`
2. Install glidertools from from the git repo with `pip install -e GliderTools`. This will allow changes you make locally, to be reflected when you import the package in Python

##### Recommended, but optional packages
There are some packages that are not installed by default, as these are large packages or can result in installation errors, resulting in failure to install GliderTools. These should install automatically with `pip install <package_name>`:
 - `gsw`: accurate density calculation (may fail in some cases)
 - `pykrige`: variogram plotting (installation generally works, except when bundled)
 - `plotly`: interactive 3D plots (large package)
